﻿# DREAMS OF ELLIOT

# DESCRIPTION
Game made in 48h to the GameJam+, in July 2018. 
My main role in this project was as programmer, using Unreal 4 as engine. 
The theme of the jam was free, but have chosen the Unicef Diversifier, where the goal is to atract more donators.
The player is child or a mini hero and, as everyone knows, he sees some things so much bigger than they really are, 
and the boss is not different.
The storyboard says that the player saves a child from explored labor (polishing the rich man's shoes) and the man gets angry
and starts attacking the player by trying to step on/smash him.
Unicef has a barrack around and helps the player providing throwable boxes and health.
In the end the player is inveted to donate do Unicef and help it the same way it helped him.

# TEAM
Programmers: Marcelino Borges
3D Artists: Caio Filizola, Eduardo Amorim, Leonardo Moura, Gabriel Matos
2D Artist: Flávio Mattos
Manager & Game Designer: Caio Filizola

# ArtStation

https://www.artstation.com/artwork/lJAKk

#YOUTUBE
https://youtu.be/6Io9KXWaJ1s
